import '../../node_modules/normalize.css/normalize.css';
import '../styles/style.scss';

import {
	createWorkspace,
	createUIRect
} from "./modules/ui-engine/api";
import { canvasId } from "./config/config";
import { createMousePointer } from "./modules/mouse/MousePointer";


/**
 * Bootstrap application
 */
const workspace = createWorkspace();


// Layer 1
const uiRect = createUIRect(0, 0, 100, 100);
workspace.addUIObjectToLayer(uiRect, 0);

// Layer 2
const layer2 = workspace.addLayer();
const uiRect2 = createUIRect(50, 50, 100, 100, {
	isFilled: true,
	fillColor: 'yellow'
});
workspace.addUIObjectToLayer(uiRect2, layer2);

// Layer 3

const layer3 = workspace.addLayer();
const uiRect3 = createUIRect(70, 70, 100, 100, {
	isFilled: true,
	fillColor: 'green'
});
workspace.addUIObjectToLayer(uiRect3, layer3);


workspace.draw();

// workspace.mountUIObject(uiObject);

