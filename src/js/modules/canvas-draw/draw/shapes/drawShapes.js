
export const drawUiRect = (ctx2d, uiRect) => {
	console.log(uiRect);
	if (uiRect.hasStroke) {
		ctx2d.lineWidth = uiRect.lineWidth;
		ctx2d.strokeRect(uiRect.x, uiRect.y, uiRect.width, uiRect.height);
	}

	if (uiRect.isFilled) {
		ctx2d.fillStyle = uiRect.fillColor;
		ctx2d.fillRect(uiRect.x, uiRect.y, uiRect.width, uiRect.height);
	}

}
