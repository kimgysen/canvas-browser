/**
 * Canvas drawing functionality
 */
import {renderUIEngineWorkspace} from "../ui-engine-client";


export const renderWorkspace = (workspace) => {
	renderUIEngineWorkspace(workspace);
}
