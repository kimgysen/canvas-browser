import {
	addWindowResizeListener,
	resizeCanvas
} from "./resizeWindow";
import {createCanvasDOM} from "../dom";


/**
 * initCanvas
 * @param $canvas: {HTMLCanvasElement} Canvas dom element
 * @param dispatcher: { Dispatcher } Event dispatcher
 * @returns {{ctx2d: ImageBitmapRenderingContext | WebGLRenderingContext | WebGL2RenderingContext | CanvasRenderingContext2D | RenderingContext | OffscreenRenderingContext | OffscreenCanvasRenderingContext2D}}
 */
const createFullWidthCanvas = ($canvas, dispatcher) => {
	addWindowResizeListener($canvas, dispatcher);
	resizeCanvas($canvas, dispatcher);

	return {
		ctx2d: $canvas.getContext("2d")
	};

}

export const startFullScreenCanvas = dispatcher => {
	const $canvas = createCanvasDOM();
	const { ctx2d } = createFullWidthCanvas($canvas, dispatcher);
	return { $canvas, ctx2d };
};

// TODO: Implement startup with fixed dimensions
export const startCanvasWithDimensions = () => {

};
