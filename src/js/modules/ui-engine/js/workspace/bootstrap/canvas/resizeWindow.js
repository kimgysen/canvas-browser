
import { ON_RESIZE_CANVAS } from "../../../event-dispatcher/events";


const scaleCanvasFullScreen = $canvas => {
	$canvas.width = window.innerWidth;
	$canvas.height = window.innerHeight;

}

export const resizeCanvas = ($canvas, dispatcher)  => {
	scaleCanvasFullScreen($canvas);

	dispatcher.dispatchEvent({
		eventType: ON_RESIZE_CANVAS,
		evt: {
			width: $canvas.getBoundingClientRect().width,
			height: $canvas.getBoundingClientRect().height
		}
	});

}

let doIt;
export const addWindowResizeListener = ($canvas, dispatcher) => {
	window.onresize = () => {
		clearTimeout(doIt);
		doIt = setTimeout(() => {
			resizeCanvas($canvas, dispatcher);
		}, 100);
	}
}
