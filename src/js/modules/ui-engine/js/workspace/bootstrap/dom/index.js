import { createAndAttachEl } from "../../../../../utils/dom-alias";

import { canvasId } from '../../../../../../config/config';


const createCanvasElement = id =>
	createAndAttachEl({
		type: 'canvas',
		id,
		$attachTo: document.body
	});

export const createCanvasDOM = () => createCanvasElement(canvasId);
