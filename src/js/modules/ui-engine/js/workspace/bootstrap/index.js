import EventDispatcher from "../../../../utils/event-dispatcher/dispatcher";
import { events, listeners } from '../../event-dispatcher';
import { startFullScreenCanvas } from "./canvas";
import CanvasArea from "../area/CanvasArea";
import LayerManager from "../layer/LayerManager";
import MousePointer from "../../../../mouse/MousePointer";

import Workspace from "../Workspace";


const createDispatcher = () => new EventDispatcher(events, listeners);

const createCanvasArea = dispatcher => new CanvasArea(0, 0, 0, 0, dispatcher);

const createLayerManager = ({ ctx2d, canvasArea }) => new LayerManager({ ctx2d, canvasArea });

const createMousePointer = $domEl => new MousePointer($domEl);


// Bootstrap workspace with custom dependency injection
export const createWorkspace = (options) => {

	const dispatcher = createDispatcher();
	const canvasArea = createCanvasArea(dispatcher);
	const { ctx2d, $canvas } = startFullScreenCanvas(dispatcher);

	const mousePointer = createMousePointer($canvas);
	mousePointer.registerMouseMove();
	mousePointer.registerClick();

	const layerManager = createLayerManager({ ctx2d, canvasArea, mousePointer });

	return new Workspace({ canvasArea, layerManager, dispatcher });

}
