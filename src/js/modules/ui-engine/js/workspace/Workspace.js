
import {ON_RESIZE_CANVAS} from "../event-dispatcher/events";


/**
 * Manage the available and occupied space in the canvas (space coverage)
 */

export default class Workspace {

	constructor({ canvasArea, layerManager, dispatcher, mousePointer }) {
		//TODO: Options fullscreen vs fixed dimensions
		this._dispatcher = dispatcher;
		this._layerManager = layerManager;
		this._canvasArea = canvasArea;
		this._mousePointer = mousePointer;

		this._registerWindowResizeListener()
	}

	_registerWindowResizeListener() {
		const _onWindowResize = () => {
			this.draw();
		}

		this._dispatcher.registerListener({
			eventType: ON_RESIZE_CANVAS,
			listener: _onWindowResize });
	}

	addLayer() {
		return this._layerManager.addLayer();
	}

	draw(){
		this._layerManager.drawLayers();
	}

	addUIObjectToLayer(uiObject, layerIndex) {
		this._layerManager.addUIObjectToLayer(uiObject, layerIndex);
	};

}
