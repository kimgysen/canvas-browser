
import Layer from "./Layer";
import {clearCanvas} from "../../../../canvas-draw/draw/clear";

export default class LayerManager {

	constructor({ ctx2d, canvasArea, mousePointer }) {
		this._ctx2d = ctx2d;
		this._canvasArea = canvasArea;
		this._mousePointer = mousePointer;

		this._layers = [];
		this.currentLayerIndex = 0;

		this._addDefaultLayer();
	}

	_addDefaultLayer() {
		this.addLayer(new Layer(this._ctx2d, this._canvasArea));
	}

	_getLayerCount() {
		return this._layers.length;
	}

	_getLayer(layerIndex) {
		return this._layers[layerIndex];
	}

	addLayer() {
		this._layers.push(new Layer(this._ctx2d, this._canvasArea));
		return this._getLayerCount() - 1;
	}

	insertLayerAtIndex(layer, index) {
		this._layers.splice(index, 0, layer);
	}

	removeLayerAtIndex(layer, index){
		this._layers.splice(index, 1);
	}

	addUIObjectToLayer(uiObject, layerIndex) {
		this._getLayer(layerIndex).addUiObject(uiObject);
	}

	selectLayer(layerIndex) {
		this._currentLayerIndex = layerIndex;
	}

	getSelectedLayer() {
		return this._layers[this._currentLayerIndex];
	}

	drawLayer(layerIndex) {
		this._getLayer(layerIndex).render(this._ctx2d, this._canvasArea);
	}

	drawLayers() {
		clearCanvas(this._ctx2d, this._canvasArea);
		for(let layer of this._layers) {
			layer.draw();
		}
	}

}
