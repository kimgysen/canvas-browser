
import {drawUiRect} from "../../../../canvas-draw/draw/shapes/drawShapes";


export default class Layer {

	constructor(ctx2d, canvasArea) {
		this.ctx2d = ctx2d;
		this.canvasArea = canvasArea;

		this.uiObjects = [];
	}

	countUiObjects() {
		return this.uiObjects.length;
	}

	addUiObject(uiObject) {
		this.uiObjects.push(uiObject);
		return this.countUiObjects();
	}

	removeUiObjectAtIndex(index) {
		this.uiObjects.splice(index, 1);
	}

	calculateIntersections() {

	}

	drawUiObjects() {
		for(let uiObject of this.uiObjects) {
			drawUiRect(this.ctx2d, uiObject);
		}
	}

	draw() {
		this.drawUiObjects(this.ctx2d, this.uiObjects[0]);
	}
}
