/**
 * This class represents an abstract area in the plane
 */

export default class Area {

	constructor(x, y , width, height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

}

