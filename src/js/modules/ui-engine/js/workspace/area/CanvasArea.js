import {ON_RESIZE_CANVAS} from "../../event-dispatcher/events";
import Area from "./Area";

/**
 * This class represents the full canvas area in the screen
 */

export default class CanvasArea extends Area {

	constructor(x = 0, y = 0, width = 0, height = 0, dispatcher) {
		super(x, y, width, height);
		this._dispatcher = dispatcher;

		this._registerWindowResizeListener();
	}

	_registerWindowResizeListener() {
		const _onWindowResize = ({ width, height }) => {
			this.width = width;
			this.height = height;
		}

		this._dispatcher.registerListener({
			eventType: ON_RESIZE_CANVAS,
			listener: _onWindowResize });
	}

}
