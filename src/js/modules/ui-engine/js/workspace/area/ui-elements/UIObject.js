/**
 * The root UIObject that all types of UI object inherit
 */
import Area from "../Area";

export default class UIObject extends Area {

	constructor(x = 0, y = 0, width, height) {
		super(x, y, width, height);

		this.isDraggable = false;
	}

	setDraggable(isDraggable) {
		this.isDraggable = isDraggable;
	}

}
