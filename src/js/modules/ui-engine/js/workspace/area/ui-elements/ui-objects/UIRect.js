import UIObject from "../UIObject";

export default class UIRect extends UIObject {

	constructor(x, y, width, height, options = {
		hasStroke: true,
		lineWidth: 1,
		lineColor: '#000000',
		isFilled: false,
		fillColor: '#FFFFFF'
	}) {
		super(x, y, width, height);
		this.hasStroke = options.hasStroke;
		this.lineWidth = options.lineWidth;
		this.lineColor = options.lineColor;
		this.isFilled = options.isFilled;
		this.fillColor = options.fillColor;
	}

	isHovered() {

	}

}