
import { createWorkspace } from "../js/workspace/bootstrap/index";
import UIRect from "../js/workspace/area/ui-elements/ui-objects/UIRect";

/**
 * This declares the public api for use
 */

// Workspace
export { createWorkspace };

// UIObjects
export const createUIRect =
	(x, y, width, height, options) => new UIRect(x, y, width, height, options);

