
import { throttle } from "../utils/performance/throttle";
import {removeItemFromArray} from "../utils/array";


const MOUSEMOVE_THROTTLE_DELAY = 200;

export default class MousePointer {

	constructor($domEl) {
		this._$domEl = $domEl;
		this._events = [];
	}

	getEvents() {
		return this._events;
	}

	registerMouseMove() {
		this._$domEl.addEventListener("mousemove", throttle(MOUSEMOVE_THROTTLE_DELAY, this.onMouseMove.bind(this)));
		this._events.push("mousemove");
	}

	unregisterMouseMove() {
		this._$domEl.removeEventListener("mousemove", this.onMouseMove);
		removeItemFromArray(this._events, "mousemove");
	}

	onMouseMove(e) {
		this.x = e.clientX;
		this.y = e.clientY;
		console.log(this.x, this.y);
	}

	registerClick() {
		this._$domEl.addEventListener("click", this.onClick.bind(this));
		this._events.push("click");
	}

	unregisterClick() {
		removeItemFromArray(this._events, "click");
	}

	onClick(e) {
		const x = e.clientX;
		const y = e.clientY;
		console.log(`clicked x: ${ x }, y: ${ y }`);
	}



}