

export default class Dispatcher {

	constructor(events, listeners) {
		this._events = events;
		this._listeners = listeners;
	}

	_eventTypeExists(eventType) {
		return eventType in this._events;
	}

	_eventTypeExistsOrThrow(eventType) {
		if (!this._eventTypeExists(eventType)) {
			throw new Error(`Event type ${ eventType } does not exist.`);
		}
	}

	registerListener({ eventType, listener }) {
		this._eventTypeExistsOrThrow(eventType);
		this._listeners[eventType].push(listener);
	}

	unregisterListener({ eventType, listener }) {
		this._eventTypeExistsOrThrow(eventType);
		this._listeners[eventType].filter(
			evtListener => listener.toString() !== evtListener.toString());
	}

	dispatchEvent({ eventType, evt }) {
		this._eventTypeExistsOrThrow(eventType);
		this._listeners[eventType].forEach(listener =>
			listener(evt));
	}

}
