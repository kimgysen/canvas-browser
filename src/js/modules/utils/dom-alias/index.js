
/**
 * Helper module to shorten DOM api
 */

export const createAndAttachEl = ({ type, id, $attachTo }) => {
	const $el = createDomElementWithId({ type, id })
	$attachTo.appendChild($el);
	return $el;
};

export const createDomElementWithId = ({ type, id }) => {
	const $el = document.createElement(type);
	if (id) $el.setAttribute('id', id);
	return $el;
}
