
import { deepClone } from "../clone";

/**
 * Utility to override default options in a function
 */

export const overrideOptions = ({ defaultOptions, options = {} }) => {
	const clonedDefaults = deepClone(defaultOptions);
	for (const [key, value] of Object.entries(defaultOptions)) {
		if (key in options) {
			clonedDefaults[key] = value;
		}
	}
	return clonedDefaults;
};
